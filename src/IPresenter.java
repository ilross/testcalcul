import java.io.IOException;
import java.util.ArrayList;

public interface IPresenter {
    public void getResult (String massage);
    public void getSqrt(String massage);
    public void getSign(String massage);
    public void getRecord(ArrayList<String> line) throws IOException;
}