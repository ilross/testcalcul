import model.ListHistory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class View extends JFrame implements IView {

    static JFrame jFrame;
    static JPanel jPanel;
    static JPanel jPanelHistory;
    static String textPanel=" ";
    static JTextField jTextField;
    static ArrayList <String> History=new ArrayList();
    static String historyPanel=" ";
    static IPresenter presenter;
    static JFrame jFrameHistory;

    private void initPresenter(){
        presenter = new Presenter(this);
    }




    public static void main(String[] args) {
        View view = new View();
        view.initPresenter();
        jFrame = new JFrame("Practice Calc");
        jFrameHistory=new JFrame("История");
        jPanel = new JPanel();
        jPanel.setLayout(null);
        jPanelHistory=new JPanel();
        jFrameHistory=new JFrame("История");
        jPanel.setLayout(null);

        jFrameHistory.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        jFrameHistory.setSize(500,700);
        jFrameHistory.setLocationRelativeTo(null);
        jFrameHistory.add(jPanelHistory);
        JLabel JLabel=new JLabel();


        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(300,400);
        jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);
        jFrame.add(jPanel);

        JMenuBar jMenuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        file.add(new JMenuItem("Save")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    presenter.getRecord(ListHistory.getInstance().getArray());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        file.add(new JMenuItem("Open")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Выберите файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    FileReader fr= null;
                    try {
                        fr = new FileReader(file);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    Scanner scan= new Scanner(fr);
                    while(scan.hasNextLine()){
                    String temp;
                    temp=scan.nextLine();
                    ListHistory.getInstance().addToArray(temp);

                    }
                    try {
                        fr.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                }
            }
        });
        JMenu history = new JMenu("History");
        history.add(new JMenuItem("Open")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jPanelHistory.add(JLabel);
                jFrameHistory.revalidate();
                jFrameHistory.setVisible(true);
                ArrayList<String> List=ListHistory.getInstance().getArray();
                String appendAll="";
                for (int i=0;i<List.size();i++){
                    appendAll+="<html>"+List.get(i) + ";<br></html>";
                }
                System.out.println(appendAll);
                JLabel.setText(appendAll);
            }
        });
        history.add(new JMenuItem("Clear")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ListHistory.getInstance().getArray().clear();
            }
        });


        jMenuBar.add(file);
        jMenuBar.add(history);
        jFrame.setJMenuBar(jMenuBar);
        jFrame.revalidate();


        jTextField = new JTextField();
        jTextField.setBounds(20,15, 260, 20);
        jTextField.setHorizontalAlignment(4);
        jPanel.add(jTextField);



        JButton jButtonBackspace = new JButton("<");
        jButtonBackspace.setBounds(20,100,50,30);
        jPanel.add(jButtonBackspace);
        jButtonBackspace.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel = textPanel.substring(0,textPanel.length() - 1);
                jTextField.setText(textPanel);
            }
        });



        JButton jButton7 = new JButton("7");
        jButton7.setBounds(20,150,50,30);
        jPanel.add(jButton7);
        jButton7.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="7";
                historyPanel+="7";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton8 = new JButton("8");
        jButton8.setBounds(90,150,50,30);
        jPanel.add(jButton8);
        jButton8.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="8";
                historyPanel+="8";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton9 = new JButton("9");
        jButton9.setBounds(160,150,50,30);
        jPanel.add(jButton9);
        jButton9.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="9";
                historyPanel+="9";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton4 = new JButton("4");
        jButton4.setBounds(20,200,50,30);
        jPanel.add(jButton4);
        jButton4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="4";
                historyPanel+="4";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton5 = new JButton("5");
        jButton5.setBounds(90,200,50,30);
        jPanel.add(jButton5);
        jButton5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="5";
                historyPanel+="5";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton6 = new JButton("6");
        jButton6.setBounds(160,200,50,30);
        jPanel.add(jButton6);
        jButton6.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="6";
                historyPanel+="6";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton1 = new JButton("1");
        jButton1.setBounds(20,250,50,30);
        jPanel.add(jButton1);
        jButton1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="1";
                historyPanel+="1";
             jTextField.setText(textPanel);
            }
        });


        JButton jButton2 = new JButton("2");
        jButton2.setBounds(90,250,50,30);
        jPanel.add(jButton2);
        jButton2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="2";
                historyPanel+="2";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton3 = new JButton("3");
        jButton3.setBounds(160,250,50,30);
        jPanel.add(jButton3);
        jButton3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="3";
                historyPanel+="3";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton0 = new JButton("0");
        jButton0.setBounds(20,300,120,30);
        jPanel.add(jButton0);
        jButton0.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="0";
                historyPanel+="0";
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonDot = new JButton(".");
        jButtonDot.setBounds(160,300,50,30);
        jPanel.add(jButtonDot);
        jButtonDot.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+=".";
                historyPanel+=".";
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonPlus = new JButton("+");
        jButtonPlus.setBounds(230,150,50,80);
        jPanel.add(jButtonPlus);
        jButtonPlus.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="+";
                historyPanel+="+";
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonMinus = new JButton("-");
        jButtonMinus.setBounds(230,100,50,30);
        jPanel.add(jButtonMinus);
        jButtonMinus.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="-";
                historyPanel+="-";
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonMultiply = new JButton("*");
        jButtonMultiply.setBounds(160,100,50,30);
        jPanel.add(jButtonMultiply);
        jButtonMultiply.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="*";
                historyPanel+="*";
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonDivide = new JButton("/");
        jButtonDivide.setBounds(90,100,50,30);
        jPanel.add(jButtonDivide);
        jButtonDivide.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textPanel+="/";
                historyPanel+="/";
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonPosOrNeg = new JButton("±");
        jButtonPosOrNeg.setBounds(230,50,50,30);
        jPanel.add(jButtonPosOrNeg);
        jButtonPosOrNeg.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                presenter.getSign(textPanel);
                textPanel=jTextField.getText();
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonEqual = new JButton("=");
        jButtonEqual.setBounds(230,250,50,80);
        jPanel.add(jButtonEqual);
        jButtonEqual.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)  {
                presenter.getResult(textPanel);
                historyPanel+="="+jTextField.getText();
                ListHistory.getInstance().addToArray(historyPanel);
                textPanel=jTextField.getText();
                jTextField.setText(textPanel);
                historyPanel = "";
            }
        });

        JButton jButtonSqrt = new JButton("v");
        jButtonSqrt.setBounds(160,50,50,30);
        jPanel.add(jButtonSqrt);
        jButtonSqrt.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                presenter.getSqrt(textPanel);
                textPanel=jTextField.getText();
                jTextField.setText(textPanel);
            }
        });



        jFrame.revalidate();

        jFrame.setVisible(true);
        System.out.println(textPanel);

    }


    @Override
    public void showMassage(String massage) {
        jTextField.setText(massage);
    }

    @Override
    public void showMassage(ArrayList<String > massage) {

    }


}