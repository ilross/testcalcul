import model.ListHistory;
import model.Model;

import java.io.IOException;
import java.util.ArrayList;

public class Presenter implements IPresenter{

    IView v;
    Model model;

    public Presenter(IView view) {
        this.v = view;
        model=new Model();
    }

    @Override
    public void getResult(String massage) {
        v.showMassage(model.equal(massage));
    }
    @Override
    public void getSqrt(String massage) {v.showMassage(model.sqrt(massage)); }
    public void getSign(String massage){
        v.showMassage(model.changeSign(massage));
    }
    public void getRecord(ArrayList<String > line)throws IOException  {
        ArrayList<String> list = ListHistory.getInstance().getArray();
        v.showMassage(model.fileRecord(list));
    }

}