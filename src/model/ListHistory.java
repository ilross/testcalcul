package model;

import java.util.ArrayList;
import java.util.List;

public class ListHistory {

    private static ListHistory listHistory;
    private static ArrayList<String> list;

    public static ListHistory getInstance(){
        if(listHistory == null){
            listHistory = new ListHistory();
        }
        return listHistory;
    }

    private ListHistory() {
        list = new ArrayList<>();
    }

    public ArrayList<String> getArray(){
        return this.list;
    }

    public void addToArray(String num){
        list.add(num);
    }
}
