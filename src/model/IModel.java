package model;

import java.io.IOException;
import java.util.ArrayList;

public interface IModel {
    public String equal(String line);
    public String sqrt(String line);
    public String changeSign(String line);
    public ArrayList<String > fileRecord(ArrayList<String > history ) throws IOException ;
}
