package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Model implements IModel {

    public Model() {
        ListHistory history = ListHistory.getInstance();
    }

    @Override
    public String equal(String line) {
        if (line.contains("+")) {
            String[] temp = line.split("\\+");
            if (line.contains(".")) {
                if (temp[0].contains(".") && temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 + num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[0].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    int num2 = Integer.parseInt(part2.trim());
                    double result = num1 + num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    int num1 = Integer.parseInt(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 + num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                }
            } else {
                String part1 = temp[0];
                String part2 = temp[1];
                int num1 = Integer.parseInt(part1.trim());
                int num2 = Integer.parseInt(part2.trim());
                int result = num1 + num2;
                String temp_2 = Integer.toString(result);
                line = temp_2;
            }
        } else if (line.contains("-")) {
            String[] temp = line.split("\\-");
            if (line.contains(".")) {
                if (temp[0].contains(".") && temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 - num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[0].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    int num2 = Integer.parseInt(part2.trim());
                    double result = num1 - num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    int num1 = Integer.parseInt(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 - num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                }
            } else {
                String part1 = temp[0];
                String part2 = temp[1];
                int num1 = Integer.parseInt(part1.trim());
                int num2 = Integer.parseInt(part2.trim());
                int result = num1 - num2;
                String temp_2 = Integer.toString(result);
                line = temp_2;
            }
        } else if (line.contains("*")) {
            String[] temp = line.split("\\*");
            if (line.contains(".")) {
                if (temp[0].contains(".") && temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 * num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[0].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    int num2 = Integer.parseInt(part2.trim());
                    double result = num1 * num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    int num1 = Integer.parseInt(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 * num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                }
            } else {
                String part1 = temp[0];
                String part2 = temp[1];
                int num1 = Integer.parseInt(part1.trim());
                int num2 = Integer.parseInt(part2.trim());
                int result = num1 * num2;
                String temp_2 = Integer.toString(result);
                line = temp_2;
            }
        } else if (line.contains("/")) {
            String[] temp = line.split("\\/");
            if (line.contains(".")) {
                if (temp[0].contains(".") && temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 / num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[0].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    double num1 = Double.parseDouble(part1.trim());
                    int num2 = Integer.parseInt(part2.trim());
                    double result = num1 / num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                } else if (temp[1].contains(".")) {
                    String part1 = temp[0];
                    String part2 = temp[1];
                    int num1 = Integer.parseInt(part1.trim());
                    double num2 = Double.parseDouble(part2.trim());
                    double result = num1 / num2;
                    String temp_2 = Double.toString(result);
                    line = temp_2;
                }
            } else {
                String part1 = temp[0];
                String part2 = temp[1];
                int num1 = Integer.parseInt(part1.trim());
                int num2 = Integer.parseInt(part2.trim());
                int result = num1 / num2;
                String temp_2 = Integer.toString(result);
                line = temp_2;
            }
        }
        return line;
    }

    public String sqrt(String line) {
        double temp = Double.parseDouble(line.trim());
        temp = Math.sqrt(temp);
        line = Double.toString(temp);
        return line;
    }

    @Override
    public String changeSign(String line) {
        int temp = Integer.parseInt(line.trim());
        temp *= -1;
        line = Integer.toString(temp);
        return line;
    }

    public ArrayList<String> fileRecord(ArrayList<String> history) throws IOException {
        File file = new File("History.txt");
        file.createNewFile();
        FileWriter fw = new FileWriter(file);
        for (int i = 0; i < history.size(); i++) {
            fw.write(history.get(i) + "\n");
        }
        fw.flush();
        fw.close();

    return history;
    }
}
